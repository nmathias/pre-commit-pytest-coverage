pytest mirror
============

Mirror of pytest package for pre-commit.


### Using pytest with pre-commit

Add this to your `.pre-commit-config.yaml`:

```yaml
-   repo: https://github.com/matngo/pre-commit-pytest-cov
    rev: 'master'
    hooks:
    -   id: pytest
```
